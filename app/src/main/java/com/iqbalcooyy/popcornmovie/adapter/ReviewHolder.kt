package com.iqbalcooyy.popcornmovie.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.iqbalcooyy.popcornmovie.BuildConfig
import com.iqbalcooyy.popcornmovie.R
import com.iqbalcooyy.popcornmovie.data.models.ReviewDetailModel
import com.iqbalcooyy.popcornmovie.databinding.ItemReviewBinding

class ReviewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val binding = ItemReviewBinding.bind(view)

    fun bindView(review: ReviewDetailModel) = with(itemView) {
        review.authorDetails?.let { details ->
            //show image
            details.authorAvatar?.let {
                Glide.with(this)
                    .load("${BuildConfig.END_POINT_IMG}$it")
                    .error(R.drawable.ic_image_flat)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(binding.imgAvatar)
            }

            binding.tvReviewRating.text = details.authorRating
        }

        binding.tvReviewerName.text = review.authorName
        binding.tvReviewDetail.text = review.reviewContent
    }
}