package com.iqbalcooyy.popcornmovie.adapter

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.iqbalcooyy.popcornmovie.BuildConfig
import com.iqbalcooyy.popcornmovie.R
import com.iqbalcooyy.popcornmovie.data.models.MovieDiscoverDetailModel
import com.iqbalcooyy.popcornmovie.databinding.ItemMovieBinding
import com.iqbalcooyy.popcornmovie.ui.MovieDetailsActivity

class MovieHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val binding = ItemMovieBinding.bind(view)

    fun bindView(movieDiscoverItem: MovieDiscoverDetailModel) = with(itemView) {
        binding.tvMovieName.text = movieDiscoverItem.originalTitle
        binding.tvRating.text = movieDiscoverItem.voteAverage
        binding.tvReleaseDate.text = movieDiscoverItem.releaseDate
        binding.tvNoAdult.text = "Adult: ${movieDiscoverItem.adult}"

        // show image
        Glide.with(this)
            .load("${BuildConfig.END_POINT_IMG}${movieDiscoverItem.posterImg}")
            .error(R.drawable.ic_image_flat)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(binding.imgMovieList)

        setOnClickListener {
            val intent = Intent(context, MovieDetailsActivity::class.java)
            intent.putExtra("_movieID", movieDiscoverItem.movieID)
            context.startActivity(intent)
        }
    }
}