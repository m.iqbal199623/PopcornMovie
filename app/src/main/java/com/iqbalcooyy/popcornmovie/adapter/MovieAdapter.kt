package com.iqbalcooyy.popcornmovie.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.iqbalcooyy.popcornmovie.data.models.MovieDiscoverDetailModel
import com.iqbalcooyy.popcornmovie.databinding.ItemMovieBinding

class MovieAdapter(private val movieDiscoverList: List<MovieDiscoverDetailModel>) :
    RecyclerView.Adapter<MovieHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val viewBinding = ItemMovieBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return MovieHolder(viewBinding.root)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        holder.bindView(movieDiscoverList[position])
    }

    override fun getItemCount(): Int = movieDiscoverList.size
}
