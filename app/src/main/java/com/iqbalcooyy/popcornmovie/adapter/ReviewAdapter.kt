package com.iqbalcooyy.popcornmovie.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.iqbalcooyy.popcornmovie.data.models.ReviewDetailModel
import com.iqbalcooyy.popcornmovie.databinding.ItemReviewBinding

class ReviewAdapter(private val reviewList: List<ReviewDetailModel>) :
    RecyclerView.Adapter<ReviewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewHolder {
        val viewBinding = ItemReviewBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return ReviewHolder(viewBinding.root)
    }

    override fun onBindViewHolder(holder: ReviewHolder, position: Int) {
        holder.bindView(reviewList[position])
    }

    override fun getItemCount(): Int = reviewList.size
}
