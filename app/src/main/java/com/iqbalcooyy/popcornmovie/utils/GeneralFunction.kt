package com.iqbalcooyy.popcornmovie.utils

import android.app.Activity
import android.app.Dialog
import com.iqbalcooyy.popcornmovie.databinding.LoadingViewBinding

class GeneralFunction {
    companion object {

        fun Dialog.showCustomDialog(activity: Activity) {
            val binding = LoadingViewBinding.inflate(activity.layoutInflater)
            val layout = binding.root

            this.apply {
                setContentView(layout)
                setCancelable(false)
                show()
            }
        }

    }
}