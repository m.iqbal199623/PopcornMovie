package com.iqbalcooyy.popcornmovie.utils

sealed class UiState<out T> {
    data class Loading<out T>(val state: Boolean) : UiState<T>()
    data class Success<out T>(val data: T) : UiState<T>()
    data class Failure<out T>(val throwable: Throwable) : UiState<T>()
}
