package com.iqbalcooyy.popcornmovie.utils

class Constants {
    companion object {
        const val TIMEOUT_MSG = "Batas waktu permintaan telah habis, silahkan coba kembali"
    }
}