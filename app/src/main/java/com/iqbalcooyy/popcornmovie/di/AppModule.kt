package com.iqbalcooyy.popcornmovie.di

import com.iqbalcooyy.popcornmovie.data.repository.MovieRepo
import com.iqbalcooyy.popcornmovie.data.repository.MovieRepoImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideMovieData(): MovieRepo {
        return MovieRepoImpl()
    }

}