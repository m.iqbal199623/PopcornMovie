package com.iqbalcooyy.popcornmovie.data.models

import com.google.gson.annotations.SerializedName

data class MovieDiscoverHeaderModel(
    @SerializedName("results")
    val results: List<MovieDiscoverDetailModel>? = emptyList()
)
