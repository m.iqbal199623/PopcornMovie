package com.iqbalcooyy.popcornmovie.data.models

import com.google.gson.annotations.SerializedName

data class ReviewDetailModel(
    @SerializedName("author")
    val authorName: String? = "",

    @SerializedName("author_details")
    val authorDetails: AuthorReviewModel? = AuthorReviewModel(),

    @SerializedName("content")
    val reviewContent: String? = ""
)
