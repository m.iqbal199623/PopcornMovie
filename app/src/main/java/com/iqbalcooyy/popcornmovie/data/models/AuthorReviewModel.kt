package com.iqbalcooyy.popcornmovie.data.models

import com.google.gson.annotations.SerializedName

data class AuthorReviewModel(
    @SerializedName("username")
    val authorUsername: String? = "",

    @SerializedName("avatar_path")
    val authorAvatar: String? = "",

    @SerializedName("rating")
    val authorRating: String? = ""
)
