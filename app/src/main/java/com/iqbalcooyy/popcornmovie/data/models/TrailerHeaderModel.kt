package com.iqbalcooyy.popcornmovie.data.models

import com.google.gson.annotations.SerializedName

data class TrailerHeaderModel(
    @SerializedName("results")
    val results: List<TrailerDetailModel>? = emptyList()
)
