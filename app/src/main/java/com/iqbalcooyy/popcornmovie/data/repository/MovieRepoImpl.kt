package com.iqbalcooyy.popcornmovie.data.repository

import com.iqbalcooyy.popcornmovie.data.models.*
import com.iqbalcooyy.popcornmovie.network.NetworkConfig
import com.iqbalcooyy.popcornmovie.utils.Constants
import com.iqbalcooyy.popcornmovie.utils.UiState
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException

class MovieRepoImpl : MovieRepo {

    override fun getGenres(
        endPoint: String,
        apiKey: String,
        callback: (UiState<GenresHeaderModel>) -> Unit
    ) {
        NetworkConfig().api(endPoint).getGenres(apiKey)
            .enqueue(object : Callback<GenresHeaderModel> {
                override fun onResponse(
                    call: Call<GenresHeaderModel>,
                    response: Response<GenresHeaderModel>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            callback(UiState.Success(it))
                        }
                    } else {
                        try {
                            val jsonConverter: JSONObject
                            response.errorBody()?.let {
                                jsonConverter = JSONObject(it.string())
                                callback(UiState.Failure(Throwable("Error ${response.code()}: ${jsonConverter["status_message"]}")))
                            }
                        } catch (e: Exception) {
                            callback(UiState.Failure(Throwable("Error ${response.code()}: ${response.message()}, ${e.message}")))
                        }
                    }
                }

                override fun onFailure(call: Call<GenresHeaderModel>, t: Throwable) {
                    if (t is SocketTimeoutException)
                        callback(UiState.Failure(Throwable(Constants.TIMEOUT_MSG)))
                    else
                        callback(UiState.Failure(Throwable(t.message)))
                }
            })
    }

    override fun discoverMovie(
        endPoint: String,
        apiKey: String,
        genreID: String,
        callback: (UiState<MovieDiscoverHeaderModel>) -> Unit
    ) {
        NetworkConfig().api(endPoint).discoverMovie(apiKey, genreID)
            .enqueue(object : Callback<MovieDiscoverHeaderModel> {
                override fun onResponse(
                    call: Call<MovieDiscoverHeaderModel>,
                    response: Response<MovieDiscoverHeaderModel>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            callback(UiState.Success(it))
                        }
                    } else {
                        try {
                            val jsonConverter: JSONObject
                            response.errorBody()?.let {
                                jsonConverter = JSONObject(it.string())
                                callback(UiState.Failure(Throwable("Error ${response.code()}: ${jsonConverter["status_message"]}")))
                            }
                        } catch (e: Exception) {
                            callback(UiState.Failure(Throwable("Error ${response.code()}: ${response.message()}, ${e.message}")))
                        }
                    }
                }

                override fun onFailure(call: Call<MovieDiscoverHeaderModel>, t: Throwable) {
                    if (t is SocketTimeoutException)
                        callback(UiState.Failure(Throwable(Constants.TIMEOUT_MSG)))
                    else
                        callback(UiState.Failure(Throwable(t.message)))
                }
            })
    }

    override fun getMovieDetails(
        endPoint: String,
        movieID: Int,
        apiKey: String,
        callback: (UiState<MovieDetailsModel>) -> Unit
    ) {
        NetworkConfig().api(endPoint).getMovieDetails(movieID, apiKey)
            .enqueue(object : Callback<MovieDetailsModel> {
                override fun onResponse(
                    call: Call<MovieDetailsModel>,
                    response: Response<MovieDetailsModel>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            callback(UiState.Success(it))
                        }
                    } else {
                        try {
                            val jsonConverter: JSONObject
                            response.errorBody()?.let {
                                jsonConverter = JSONObject(it.string())
                                callback(UiState.Failure(Throwable("Error ${response.code()}: ${jsonConverter["status_message"]}")))
                            }
                        } catch (e: Exception) {
                            callback(UiState.Failure(Throwable("Error ${response.code()}: ${response.message()}, ${e.message}")))
                        }
                    }
                }

                override fun onFailure(call: Call<MovieDetailsModel>, t: Throwable) {
                    if (t is SocketTimeoutException)
                        callback(UiState.Failure(Throwable(Constants.TIMEOUT_MSG)))
                    else
                        callback(UiState.Failure(Throwable(t.message)))
                }
            })
    }

    override fun getReviews(
        endPoint: String,
        movieID: Int,
        apiKey: String,
        callback: (UiState<ReviewHeaderModel>) -> Unit
    ) {
        NetworkConfig().api(endPoint).getReviews(movieID, apiKey)
            .enqueue(object : Callback<ReviewHeaderModel> {
                override fun onResponse(
                    call: Call<ReviewHeaderModel>,
                    response: Response<ReviewHeaderModel>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            callback(UiState.Success(it))
                        }
                    } else {
                        try {
                            val jsonConverter: JSONObject
                            response.errorBody()?.let {
                                jsonConverter = JSONObject(it.string())
                                callback(UiState.Failure(Throwable("Error ${response.code()}: ${jsonConverter["status_message"]}")))
                            }
                        } catch (e: Exception) {
                            callback(UiState.Failure(Throwable("Error ${response.code()}: ${response.message()}, ${e.message}")))
                        }
                    }
                }

                override fun onFailure(call: Call<ReviewHeaderModel>, t: Throwable) {
                    if (t is SocketTimeoutException)
                        callback(UiState.Failure(Throwable(Constants.TIMEOUT_MSG)))
                    else
                        callback(UiState.Failure(Throwable(t.message)))
                }
            })
    }

    override fun getTrailer(
        endPoint: String,
        movieID: Int,
        apiKey: String,
        callback: (UiState<TrailerHeaderModel>) -> Unit
    ) {
        NetworkConfig().api(endPoint).getTrailer(movieID, apiKey)
            .enqueue(object : Callback<TrailerHeaderModel> {
                override fun onResponse(
                    call: Call<TrailerHeaderModel>,
                    response: Response<TrailerHeaderModel>
                ) {
                    if (response.isSuccessful) {
                        response.body()?.let {
                            callback(UiState.Success(it))
                        }
                    } else {
                        try {
                            val jsonConverter: JSONObject
                            response.errorBody()?.let {
                                jsonConverter = JSONObject(it.string())
                                callback(UiState.Failure(Throwable("Error ${response.code()}: ${jsonConverter["status_message"]}")))
                            }
                        } catch (e: Exception) {
                            callback(UiState.Failure(Throwable("Error ${response.code()}: ${response.message()}, ${e.message}")))
                        }
                    }
                }

                override fun onFailure(call: Call<TrailerHeaderModel>, t: Throwable) {
                    if (t is SocketTimeoutException)
                        callback(UiState.Failure(Throwable(Constants.TIMEOUT_MSG)))
                    else
                        callback(UiState.Failure(Throwable(t.message)))
                }
            })
    }

}