package com.iqbalcooyy.popcornmovie.data.models

import com.google.gson.annotations.SerializedName

data class GenresHeaderModel(
    @SerializedName("genres")
    val genres: List<GenresDetailModel>? = emptyList()
)
