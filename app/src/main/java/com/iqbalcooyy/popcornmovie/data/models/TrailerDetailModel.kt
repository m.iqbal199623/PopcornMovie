package com.iqbalcooyy.popcornmovie.data.models

import com.google.gson.annotations.SerializedName

data class TrailerDetailModel(
    @SerializedName("key")
    val trailerKey: String? = "",

    @SerializedName("site")
    val trailerSite: String? = ""
)
