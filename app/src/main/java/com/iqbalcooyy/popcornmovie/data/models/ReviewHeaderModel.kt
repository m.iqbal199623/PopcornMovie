package com.iqbalcooyy.popcornmovie.data.models

import com.google.gson.annotations.SerializedName

data class ReviewHeaderModel(
    @SerializedName("results")
    val results: List<ReviewDetailModel>? = emptyList()
)
