package com.iqbalcooyy.popcornmovie.data.repository

import com.iqbalcooyy.popcornmovie.data.models.*
import com.iqbalcooyy.popcornmovie.utils.UiState

interface MovieRepo {

    fun getGenres(
        endPoint: String,
        apiKey: String,
        callback: (UiState<GenresHeaderModel>) -> Unit
    )

    fun discoverMovie(
        endPoint: String,
        apiKey: String,
        genreID: String,
        callback: (UiState<MovieDiscoverHeaderModel>) -> Unit
    )

    fun getMovieDetails(
        endPoint: String,
        movieID: Int,
        apiKey: String,
        callback: (UiState<MovieDetailsModel>) -> Unit
    )

    fun getReviews(
        endPoint: String,
        movieID: Int,
        apiKey: String,
        callback: (UiState<ReviewHeaderModel>) -> Unit
    )

    fun getTrailer(
        endPoint: String,
        movieID: Int,
        apiKey: String,
        callback: (UiState<TrailerHeaderModel>) -> Unit
    )

}