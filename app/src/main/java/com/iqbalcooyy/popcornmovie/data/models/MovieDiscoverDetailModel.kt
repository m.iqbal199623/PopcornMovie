package com.iqbalcooyy.popcornmovie.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDiscoverDetailModel(
    @SerializedName("id")
    val movieID: String? = "",

    @SerializedName("adult")
    val adult: Boolean? = false,

    @SerializedName("original_title")
    val originalTitle: String? = "",

    @SerializedName("overview")
    val overview: String? = "",

    @SerializedName("release_date")
    val releaseDate: String? = "",

    @SerializedName("vote_average")
    val voteAverage: String? = "",

    @SerializedName("poster_path")
    val posterImg: String? = ""
) : Parcelable
