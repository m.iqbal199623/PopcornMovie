package com.iqbalcooyy.popcornmovie.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.iqbalcooyy.popcornmovie.data.models.*
import com.iqbalcooyy.popcornmovie.data.repository.MovieRepo
import com.iqbalcooyy.popcornmovie.utils.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(private val repository: MovieRepo) : ViewModel() {

    private val _genreMovie = MutableLiveData<UiState<GenresHeaderModel>>()
    val genreMovie: LiveData<UiState<GenresHeaderModel>>
        get() = _genreMovie

    private val _discoverMovie = MutableLiveData<UiState<MovieDiscoverHeaderModel>>()
    val discoverMovieDiscover: LiveData<UiState<MovieDiscoverHeaderModel>>
        get() = _discoverMovie

    private val _movieDetails = MutableLiveData<UiState<MovieDetailsModel>>()
    val movieDetails: LiveData<UiState<MovieDetailsModel>>
        get() = _movieDetails

    private val _reviews = MutableLiveData<UiState<ReviewHeaderModel>>()
    val reviews: LiveData<UiState<ReviewHeaderModel>>
        get() = _reviews

    private val _trailers = MutableLiveData<UiState<TrailerHeaderModel>>()
    val trailers: LiveData<UiState<TrailerHeaderModel>>
        get() = _trailers

    fun getGenres(endPoint: String, apiKey: String) {
        _genreMovie.value = UiState.Loading(true)

        try {
            repository.getGenres(endPoint, apiKey) {
                _genreMovie.value = UiState.Loading(false)
                _genreMovie.value = it
            }
        } catch (e: Exception) {
            _genreMovie.value = UiState.Failure(e)
        }
    }

    fun discoverMovie(endPoint: String, apiKey: String, genreID: String) {
        _discoverMovie.value = UiState.Loading(true)

        try {
            repository.discoverMovie(endPoint, apiKey, genreID) {
                _discoverMovie.value = UiState.Loading(false)
                _discoverMovie.value = it
            }
        } catch (e: Exception) {
            _discoverMovie.value = UiState.Failure(e)
        }
    }

    fun getMovieDetails(endPoint: String, movieID: Int, apiKey: String) {
        _movieDetails.value = UiState.Loading(true)

        try {
            repository.getMovieDetails(endPoint, movieID, apiKey) {
                _movieDetails.value = UiState.Loading(false)
                _movieDetails.value = it
            }
        } catch (e: Exception) {
            _movieDetails.value = UiState.Failure(e)
        }
    }

    fun getReviews(endPoint: String, movieID: Int, apiKey: String) {
        _reviews.value = UiState.Loading(true)

        try {
            repository.getReviews(endPoint, movieID, apiKey) {
                _reviews.value = UiState.Loading(false)
                _reviews.value = it
            }
        } catch (e: Exception) {
            _reviews.value = UiState.Failure(e)
        }
    }

    fun getTrailer(endPoint: String, movieID: Int, apiKey: String) {
        _trailers.value = UiState.Loading(true)

        try {
            repository.getTrailer(endPoint, movieID, apiKey) {
                _trailers.value = UiState.Loading(false)
                _trailers.value = it
            }
        } catch (e: Exception) {
            _trailers.value = UiState.Failure(e)
        }
    }
}