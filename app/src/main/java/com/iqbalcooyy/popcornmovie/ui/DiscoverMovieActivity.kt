package com.iqbalcooyy.popcornmovie.ui

import android.app.Dialog
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.iqbalcooyy.popcornmovie.BuildConfig
import com.iqbalcooyy.popcornmovie.adapter.MovieAdapter
import com.iqbalcooyy.popcornmovie.databinding.ActivityDiscoverMovieBinding
import com.iqbalcooyy.popcornmovie.utils.Constants
import com.iqbalcooyy.popcornmovie.utils.GeneralFunction.Companion.showCustomDialog
import com.iqbalcooyy.popcornmovie.utils.UiState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DiscoverMovieActivity : AppCompatActivity() {

    private val movieVM: MovieViewModel by viewModels()
    private lateinit var viewBinding: ActivityDiscoverMovieBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewBinding = ActivityDiscoverMovieBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        // catch intent data
        val genreId = intent.getStringExtra("_genreID")
        val genreName = intent.getStringExtra("_genreName")

        // action bar
        viewBinding.actionBarDiscoverMovie.actionBarTitle.text = genreName
        viewBinding.actionBarDiscoverMovie.btnBack.setOnClickListener {
            finish()
        }

        // get discover move by genre
        genreId?.let {
            getMovie(it)
        }
    }

    private fun getMovie(genreId: String) {
        val loadingView = Dialog(this)

        // call api
        movieVM.discoverMovie(
            endPoint = BuildConfig.END_POINT,
            apiKey = BuildConfig.API_KEY,
            genreID = genreId
        )

        // observe
        movieVM.discoverMovieDiscover.observe(this) { uiState ->
            when (uiState) {
                is UiState.Loading -> {
                    if (uiState.state) {
                        loadingView.showCustomDialog(this)
                    } else {
                        loadingView.dismiss()
                    }
                }
                is UiState.Success -> {
                    uiState.data.results?.let {
                        viewBinding.rvMovieList.adapter = MovieAdapter(it)
                    }
                }
                is UiState.Failure -> {
                    uiState.throwable.message?.let {
                        if (it == Constants.TIMEOUT_MSG)
                            Toast.makeText(this, Constants.TIMEOUT_MSG, Toast.LENGTH_LONG).show()
                        else
                            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

}