package com.iqbalcooyy.popcornmovie.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.iqbalcooyy.popcornmovie.BuildConfig
import com.iqbalcooyy.popcornmovie.R
import com.iqbalcooyy.popcornmovie.data.models.GenresDetailModel
import com.iqbalcooyy.popcornmovie.databinding.ActivityMainBinding
import com.iqbalcooyy.popcornmovie.databinding.ListGenresBinding
import com.iqbalcooyy.popcornmovie.utils.Constants
import com.iqbalcooyy.popcornmovie.utils.GeneralFunction.Companion.showCustomDialog
import com.iqbalcooyy.popcornmovie.utils.UiState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val movieVM: MovieViewModel by viewModels()
    private var listGenre: MutableList<GenresDetailModel> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        // show genres
        viewBinding.btnGenreSelect.setOnClickListener {
            observeGenreApi()

            if (listGenre.size < 1)
                callGenreApi()
        }
    }

    private fun callGenreApi() {
        movieVM.getGenres(
            endPoint = BuildConfig.END_POINT,
            apiKey = BuildConfig.API_KEY
        )
    }

    private fun observeGenreApi() {
        val loadingView = Dialog(this)

        // observe view model with live data first
        movieVM.genreMovie.observe(this) { uiState ->
            when (uiState) {
                is UiState.Loading -> {
                    if (uiState.state) {
                        loadingView.showCustomDialog(this)
                    } else {
                        loadingView.dismiss()
                    }
                }
                is UiState.Success -> {
                    uiState.data.genres?.let {
                        listGenre.clear()
                        listGenre.addAll(it)
                        showGenres()
                    }
                }
                is UiState.Failure -> {
                    uiState.throwable.message?.let {
                        if (it == Constants.TIMEOUT_MSG)
                            Toast.makeText(this, Constants.TIMEOUT_MSG, Toast.LENGTH_LONG).show()
                        else
                            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun showGenres() {
        val form = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
        val formBinding = ListGenresBinding.inflate(layoutInflater)
        val mListGenre: MutableList<String> = mutableListOf()
        mListGenre.clear()

        // set data
        if (!listGenre.isNullOrEmpty()) {
            for (i in listGenre.indices) {
                listGenre[i].genreName?.let {
                    mListGenre.add(i, it)
                }
            }
        }

        // set Adapter
        val adapter = ArrayAdapter(
            this,
            R.layout.custom_list_item,
            mListGenre.distinct()
        )
        formBinding.genreList.adapter = adapter

        // listener item click
        formBinding.genreList.setOnItemClickListener { adapterView, view, position, id ->
            listGenre.find {
                it.genreName == adapterView.getItemAtPosition(position).toString()
            }?.let {
                Toast.makeText(this, "${it.genreID} - ${it.genreName}", Toast.LENGTH_LONG).show()

                val intent = Intent(this, DiscoverMovieActivity::class.java)
                intent.putExtra("_genreID", it.genreID.toString())
                intent.putExtra("_genreName", it.genreName.toString())
                startActivity(intent)
            }

            form.dismiss()
        }

        form.setContentView(formBinding.root)
        form.show()
    }
}