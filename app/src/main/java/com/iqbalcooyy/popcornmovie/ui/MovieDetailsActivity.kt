package com.iqbalcooyy.popcornmovie.ui

import android.app.Dialog
import android.os.Bundle
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.iqbalcooyy.popcornmovie.BuildConfig
import com.iqbalcooyy.popcornmovie.adapter.ReviewAdapter
import com.iqbalcooyy.popcornmovie.data.models.MovieDetailsModel
import com.iqbalcooyy.popcornmovie.databinding.ActivityMovieDetailsBinding
import com.iqbalcooyy.popcornmovie.utils.Constants
import com.iqbalcooyy.popcornmovie.utils.GeneralFunction.Companion.showCustomDialog
import com.iqbalcooyy.popcornmovie.utils.UiState
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailsActivity : AppCompatActivity() {

    private val movieVM: MovieViewModel by viewModels()
    private lateinit var viewBinding: ActivityMovieDetailsBinding
    private lateinit var loadingView: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewBinding = ActivityMovieDetailsBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        loadingView = Dialog(this)

        // catch intent data
        val movieID = intent.getStringExtra("_movieID")

        // get movie details by id
        movieID?.let {
            getMovieDetails(it)
        }
    }

    private fun getMovieDetails(movieID: String) {
        // call api
        movieVM.getMovieDetails(
            endPoint = BuildConfig.END_POINT,
            movieID = movieID.toInt(),
            apiKey = BuildConfig.API_KEY
        )

        // observe
        movieVM.movieDetails.observe(this) { uiState ->
            when (uiState) {
                is UiState.Loading -> {
                    if (uiState.state)
                        loadingView.showCustomDialog(this)
                }
                is UiState.Success -> {
                    callApiReview(uiState.data)
                }
                is UiState.Failure -> {
                    uiState.throwable.message?.let {
                        if (it == Constants.TIMEOUT_MSG)
                            Toast.makeText(this, Constants.TIMEOUT_MSG, Toast.LENGTH_LONG).show()
                        else
                            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun callApiReview(movieDetails: MovieDetailsModel) {
        var movieId = ""
        movieDetails.movieID?.let {
            movieId = it
        }

        // call api
        movieVM.getReviews(
            endPoint = BuildConfig.END_POINT,
            movieID = movieId.toInt(),
            apiKey = BuildConfig.API_KEY
        )

        // observe
        movieVM.reviews.observe(this) { uiState ->
            when (uiState) {
                is UiState.Loading -> {
                    if (uiState.state)
                        loadingView.showCustomDialog(this)
                }
                is UiState.Success -> {
                    // show trailer
                    getTrailer(movieId)

                    // set genres
                    var mGenres = ""
                    movieDetails.genres?.let { genre ->
                        for (i in genre.indices) {
                            if (i == 0)
                                mGenres = genre[i].genreName.toString()
                            else
                                mGenres += ", ${genre[i].genreName}"
                        }
                    }

                    // show details
                    viewBinding.tvMovieNameDetail.text = movieDetails.originalTitle
                    viewBinding.tvMovieRuntime.text = "${movieDetails.movieRuntime} minutes"
                    viewBinding.tvMovieRating.text = movieDetails.voteAverage
                    viewBinding.tvMovieReleaseDate.text = movieDetails.releaseDate
                    viewBinding.tvMovieGenres.text = mGenres
                    viewBinding.tvMovieOverview.text = movieDetails.overview

                    // show review
                    uiState.data.results?.let { review ->
                        viewBinding.rvReviews.adapter = ReviewAdapter(review)
                    }
                }
                is UiState.Failure -> {
                    uiState.throwable.message?.let {
                        if (it == Constants.TIMEOUT_MSG)
                            Toast.makeText(this, Constants.TIMEOUT_MSG, Toast.LENGTH_LONG).show()
                        else
                            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun getTrailer(movieID: String) {
        movieVM.getTrailer(
            endPoint = BuildConfig.END_POINT,
            movieID = movieID.toInt(),
            apiKey = BuildConfig.API_KEY
        )

        // observe
        movieVM.trailers.observe(this) { uiState ->
            when (uiState) {
                is UiState.Loading -> {
                    if (!uiState.state)
                        loadingView.dismiss()
                }
                is UiState.Success -> {
                    uiState.data.results?.let {
                        if (it.isNotEmpty()) {
                            viewBinding.webView.webViewClient = WebViewClient()
                            viewBinding.webView.loadUrl("${BuildConfig.YT_PLAY_URL}${it.last().trailerKey}")
                            viewBinding.webView.settings.javaScriptEnabled = true
                        }
                    }
                }
                is UiState.Failure -> {
                    uiState.throwable.message?.let {
                        if (it == Constants.TIMEOUT_MSG)
                            Toast.makeText(this, Constants.TIMEOUT_MSG, Toast.LENGTH_LONG).show()
                        else
                            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

}