package com.iqbalcooyy.popcornmovie.network

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NetworkConfig {

    //Configuration for Interceptor
    private fun getInterceptor(): OkHttpClient {
        val interceptor = Interceptor {
            val original = it.request()
            val requestBuilder = original.newBuilder()
            val request = requestBuilder.build()
            it.proceed(request)
        }

        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(60L, TimeUnit.SECONDS)
            .writeTimeout(60L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .build()
    }

    //Configuration for Network Library
    private fun getNetwork(baseUrl: String): Retrofit {
        val gson = GsonBuilder()
            .enableComplexMapKeySerialization()
            .setLenient() //new
            .create()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(getInterceptor())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    //Configuration for get service or request to server
    fun api(baseUrl: String): ApiService {
        return getNetwork(baseUrl).create(ApiService::class.java)
    }
}