package com.iqbalcooyy.popcornmovie.network

import com.iqbalcooyy.popcornmovie.data.models.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("genre/movie/list")
    fun getGenres(
        @Query("api_key") apiKey: String
    ): Call<GenresHeaderModel>

    @GET("discover/movie")
    fun discoverMovie(
        @Query("api_key") apiKey: String,
        @Query("with_genres") genreID: String
    ): Call<MovieDiscoverHeaderModel>

    @GET("movie/{movie_id}")
    fun getMovieDetails(
        @Path("movie_id") movieID: Int,
        @Query("api_key") apiKey: String
    ): Call<MovieDetailsModel>

    @GET("movie/{movie_id}/reviews")
    fun getReviews(
        @Path("movie_id") movieID: Int,
        @Query("api_key") apiKey: String
    ): Call<ReviewHeaderModel>

    @GET("movie/{movie_id}/videos")
    fun getTrailer(
        @Path("movie_id") movieID: Int,
        @Query("api_key") apiKey: String
    ): Call<TrailerHeaderModel>

}